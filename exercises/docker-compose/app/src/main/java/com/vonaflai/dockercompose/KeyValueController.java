package com.vonaflai.dockercompose;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/kv")
public class KeyValueController {

    private final KeyValueRepository repository;

    @PostMapping("/{key}")
    public void Set(@PathVariable @NonNull String key, @RequestBody String value)
    {
        var kv = new KeyValue(key, value);
        repository.save(kv);
    }

    @GetMapping("{key}")
    public ResponseEntity<KeyValue> Get(@PathVariable String key)
    {
        return repository.findById(key)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}
