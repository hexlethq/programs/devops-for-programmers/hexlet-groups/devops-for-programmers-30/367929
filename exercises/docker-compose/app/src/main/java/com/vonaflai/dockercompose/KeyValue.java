package com.vonaflai.dockercompose;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "kv")
public class KeyValue {

    @Id
    private String Key;

    @Column(name = "value", nullable = false)
    private String Value;
}
